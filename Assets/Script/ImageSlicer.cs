﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Texture2DをスライスしてSpriteにするクラス
/// </summary>

public class ImageSlicer : MonoBehaviour
{
    public Texture2D texture;//Texture

    List<Sprite> slicedSprite;//Spriteリスト

    public int pieceWidth;//ピース1つの幅
    public int pieceHeight;//ピース1つの高さ


    public List<Sprite> Slicer(Texture2D texture,int raw, int column)//textureをraw*column数にスライスするメソッド
    {
        this.texture = texture;//引数のtextureをfield変数に格納

        List<Texture2D> slicedTextures = new List<Texture2D>();//スライス後のテクスチャリスト
        slicedSprite = new List<Sprite>();//スプライトリスト

        pieceWidth = texture.width / raw;//幅計算
        pieceHeight = texture.height / column;//高さ計算

        for (int i = raw; i > 0; i--)
        {
            for (int h=0;h< column;h++)
            {
                
                Color[] piecePixel= PiecePixelMaker(h, i, texture);//Colorリスト生成

                Texture2D newTexture = new Texture2D (pieceWidth, pieceHeight, TextureFormat.RGBA32 , false);//新規texture定義

                newTexture.SetPixels(piecePixel);//新規textureにcolorリストセット
                newTexture.Apply();//Apply

                slicedTextures.Add(newTexture);//スライス後textureリストに格納
                
                //新sprite定義
                Sprite sprite = Sprite.Create(newTexture, new Rect(0, 0, newTexture.width, newTexture.height), new Vector2(0.5f, 0.5f));
                slicedSprite.Add(sprite);//リストに追加
            }
        }

/*        foreach(Texture2D tex in slicedTextures)//スライス後textureリストループ
        {
          
           
            
        }*/

        return slicedSprite;//スプライトリストreturn
     
    }

    Color[] PiecePixelMaker(int pieceX, int pieceY , Texture2D baseTexture)//色配列作成
    {
        var pixels = new Color[pieceWidth * pieceHeight];//分割後Sizeのcolor配列作成
        var offsetX = pieceX * pieceWidth;//ピースの開始補正位置(x
        var offsetY = pieceY * pieceHeight;//ピースの開始補正位置(y
        
        //引数で指定された範囲の色をBaseのtextureから取得してpixel[]に格納
        //baseTextureの左上を基準にループでGetpixelして色を取得
        for (int y = offsetY, yy = pieceHeight - 1; y > offsetY - pieceHeight; y--, yy--)//始点と終点の上から下ループ
        {
            for (int x = offsetX, xx = 0; x < offsetX + pieceWidth; x++, xx++)//始点と終点の左から右ループ
            {
                pixels[yy * pieceWidth + xx] = baseTexture.GetPixel(x, y);//ピクセル配列にbaseTextureの色を格納
            }
        }
        return pixels;//Color配列return
    }

}
