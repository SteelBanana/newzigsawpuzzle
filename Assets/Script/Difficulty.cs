﻿using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// 難易度(=ピースの分割数)をコントロールするクラス
/// </summary>

public class Difficulty : MonoBehaviour
{
    public GameObject GameController;//GameController
    SelectSceneController selectSceneController;//SelectSceneContoller
    Dropdown dropdown;//DropDownのUI
    [SerializeField] List<int> difficulty=new List<int>();


    // Start is called before the first frame update
    void Start()
    {
        selectSceneController = GameController.GetComponent<SelectSceneController>();//GameControllerのSelectSceneControllerを取得
        dropdown = gameObject.GetComponent<Dropdown>();//DropDownを取得
    }

    public void SetDifficulty()//難易度設定メソッド(DropdownのOnValueChangedで呼び出される)
    {
        try//難易度設定
        {
            selectSceneController.SetDifficulty(difficulty[dropdown.value - 1]);//SelectSceneControllerのSetDifficulty呼び出し(シーンまたぎ変数に難易度格納)
        }
        catch//(OutOfRangeキャッチ)
        {
            selectSceneController.SetDifficulty(0);//0
        }


    }
}
