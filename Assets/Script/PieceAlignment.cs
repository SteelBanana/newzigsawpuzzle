﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 生成したピースを分割数で座標計算して並べるクラス
/// </summary>

public class PieceAlignment : MonoBehaviour
{
    float pieceWorldWidth;//ピースのworld空間上のwidth
    float pieceWorldHeight;//ピースのworld空間上のheight
    int raw;//横分割
    int column;//縦分割
    Vector3[] pieceWorldPos = new Vector3[4];//GetWorldCorners用配列
    public GameObject GamePlayController;//gamePlayContoller


    public void Alignment()//整列メソッド
    {

        GamePlayController gamePlayController = GamePlayController.GetComponent<GamePlayController>();//GamePlayContoller取得

        int element = 0;//要素数リセット
        this.raw = gamePlayController.raw;//横分割数取得
        this.column = gamePlayController.column ;//縦分割数取得
        List<GameObject> triggerList= GamePlayController.GetComponent<GamePlayController>().triggerList;//トリガーリスト取得
        List<GameObject> pieceList = GamePlayController.GetComponent<GamePlayController>().pieceList;//pieceリスト取得

        //ピースの大きさを計算するためにGetWoeldConrersでpieceWorldPos配列に四隅の座標を格納
        pieceList[0].GetComponent<RectTransform>().GetWorldCorners(pieceWorldPos);


        pieceWorldWidth = Mathf.Abs(pieceWorldPos[0].x - pieceWorldPos[3].x) * raw;//ピースのwidth計算
        pieceWorldHeight = Mathf.Abs(pieceWorldPos[0].y - pieceWorldPos[1].y) * column;//ピースのheight計算
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(pieceWorldWidth, pieceWorldHeight);//colliderのサイズをpieceに合わせる

        int i1;//計算用変数
        int i2;//計算用変数
        int h1;//計算用変数
        int h2;//計算用変数


        //分割数が奇数か偶数かのときの座標計算用のループ変数計算
        i1 = raw / 2;
        h1 = 0 - (column / 2);
        if (raw % 2 != 0) { i2 = 0 - (i1 + 1); }
        else { i2 = 0 - i1; }
        h2 = column + h1;


        for (int i = i1; i > i2; i--)
        {
            for(int h = h1; h < h2; h++)
            {
                GameObject piece= pieceList[element];//ピース配列の[要素数]番目取得
                GameObject trigger = triggerList[element];//Trigger配列の[要素数]番目取得

                var pieceSize = piece.GetComponent<SpriteRenderer>().sprite.bounds.size;//spriteのRendererの大きさ取得

                float posX;//ピーズ生成x座標
                posX =pieceSize.x * h ;//ループ数*ピース幅
                if ((raw % 2) == 0)//ピースが偶数なら
                {
                    posX += pieceSize.x/2;//ピース半個分ずらす
                }

                float posY;//ピーズ生成y座標
                posY =pieceSize.y * i;//ループ数*y座標
                if ((column % 2) == 0)//ピースが偶数なら
                {
                    posY -= pieceSize.y/2;//ピース半個分ずらす
                }

                piece.GetComponent<RectTransform>().position = gameObject.transform.position  + new Vector3(posX, posY, 0);//ピース位置
                trigger.transform.position = piece.transform.position;//トリガーの位置をピースにあわせる
                trigger.GetComponent<BoxCollider2D>().size = piece.GetComponent<BoxCollider2D>().size * new Vector2(0.5f, 0.5f);//トリガーのColliderのサイズをpieceの半分にする
                triggerList.Add(trigger);//triggerリストに追加
                element++;//要素数インクリメント
            }
        }

    }
}
