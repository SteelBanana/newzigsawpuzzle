﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;


/// <summary>
/// GameScene全体をコントロールするクラス
/// </summary>


public class GamePlayController : MonoBehaviour
{
    public List<GameObject> pieceList = new List<GameObject>();//ピースリスト
    public List<GameObject> triggerList = new List<GameObject>();//トリガーリスト
    public Texture2D texture;//Texture
    public int column;//縦分割数
    public int raw;//横分割数
    public GameObject panel;//
    bool start=false;//起動フラグ

    private void Awake()//生成時
    {
        texture = SelectSceneController.texture;//Static変数から取得
        column = SelectSceneController.puzzleColumn;//Static変数から取得
        raw = SelectSceneController.puzzleRaw;//Static変数から取得
    }

    public void GameStart ()
    {
        if (!start)//ゲームスタート前
        {
            gameObject.GetComponent<TimeofClear>().StartTime();//タイマー起動メソッド呼び出し
            gameObject.GetComponent<Divide>().doDivide();//ピーズをバラバラに移動させるメソッド呼び出し
            panel.SetActive(false);//パネル非表示
            start = true;//フラグ変更
        }else  if (start)//ゲームスタート後(=ゲームリザルド表示時)
        {
            SceneManager.LoadScene("ImageSelectScene");//画像選択シーン呼び出し
        }

    }

}
