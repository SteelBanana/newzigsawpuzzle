﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ピースのセーブデータ型用
/// コンストラクタ
/// </summary>
[System.Serializable]
public class SavePieceBinary
{
    public byte[] binaryData;
    /*現在はバイナリデータ用byte配列だけだが
    応用が効くようにコンストラクタを定義*/
}
