﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ピースをバラバラに移動させるコルーチンを止めるクラス
/// </summary>

public class CallStopCoroutine : MonoBehaviour
{
    public GameObject gameController;//GameController


    private void OnTriggerExit2D(Collider2D collision)//ピースのCollisionがTriggerから出たら
    {
        collision.gameObject.GetComponent<MoveOut>().StopMoveOutCoroutine();//コルーチン停止メソッド呼び出し
    }
}
