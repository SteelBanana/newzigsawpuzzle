﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;



/// <summary>
/// サムネイル生成クラス
/// </summary>

public class CreateThumbnails : MonoBehaviour
{
    public GameObject thumbnailImage;//サムネイルObject
    public GameObject thumbnailPanel;//サムネイルパネル
    List<GameObject> thumbnailImages;//サムネイルObjectリスト


    void Awake()
    {
        thumbnailImage = (GameObject)Resources.Load("Prefabs/RawImage");//Prefabのロード
        thumbnailImages = gameObject.GetComponent<SelectSceneController>().thumbnailImages;//SelectSceneControllerからリスト取得
    }


    public void AddThumbnails(List<SavePieceBinary> savePieceBinary)//サムネイル追加メソッド
    {
        foreach(GameObject reset in thumbnailImages)//重複回避のための既存サムネイル削除
        {
            Destroy(reset);
        }

        thumbnailImages.Clear();//リストクリア

        foreach (SavePieceBinary save in savePieceBinary)//セーブデータリストループ
        {
            byte[] readBinary = save.binaryData;//readBinary

            //ImportButtonクラスからTesture生成メソッド呼び出し
            Texture2D texture=gameObject.GetComponent<ImportButton>().CleateTextureFromBinary(readBinary);


            GameObject newThumbnail = Instantiate(thumbnailImage);//Instantiate
            RawImage raw = newThumbnail.GetComponentInChildren<RawImage>();//サムネイルのRawImage取得
            raw.SetNativeSize();//Set
            raw.texture = texture;//RawImageのTextureに生成したtexture格納      
            newThumbnail.transform.SetParent(thumbnailPanel.transform,false);//サムネイルの親設定

            //子のToggleのグループをサムネイルの親Objectのトグルグループに設定
            newThumbnail.GetComponentInChildren<Toggle>().group = newThumbnail.transform.parent.gameObject.GetComponent<ToggleGroup>();
            thumbnailImages.Add(newThumbnail);//サムネイルリストに生成したサムネイルを追加
        }
    }
}
