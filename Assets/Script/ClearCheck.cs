﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




/// <summary>
/// 正解のピースがはまるたびに呼び出されるクラス
/// </summary>
/// 
public class ClearCheck : MonoBehaviour
{
    List<GameObject> pieces;//pieceリスト
    bool check; //クリアチェック判定
    public int Count;//カウンター

    void Start()
    {
        pieces = gameObject.GetComponent<GamePlayController>().pieceList;//GamePlayControllerからpieceListを取得
    }

    //各ピースを確認するメソッド
    public void ClearChecker()
    {
        Count = 0;//カウント初期化

        foreach (GameObject piece in pieces)//pieceリストのループ
        {
            if(!piece.GetComponent<PieceController>().cleared)//pieceのclearedチェック
            {
                break;//cleadがfalseならbreakして判断修了
            }
            Count++;//カウントインクリメント 
        }

        if(Count == pieces.Count)//カウントがpiece数と同じなら
        {
            gameObject.GetComponent<TimeofClear>().StopTime();//タイマー停止コルーチン呼び出し
        }
    }
}
