﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// パズル絵の大きさによってカメラの大きさとアス比を変更するクラス
/// </summary>


public class CameraSizeController : MonoBehaviour
{
    Camera mainCamera;//カメラ
    [SerializeField]GameObject GameController;//GameController
    Texture2D texture;//Texture
    float textureWidth;//textureのwidth
    float textureHeight;//textureのheight
    float baseWidth;//Screenのwidth
    float baseHeight;//ScreenのHeight
    float pieceWidth;//pieceのwidth
    float pieceHeight;//pieceのheight

    private void Start()
    {
        mainCamera = gameObject.GetComponent<Camera>();//カメラをgetConponent
        texture = GameController.GetComponent<ImageSlicer>().texture;//TextureをImageSlicerから取得
        textureWidth = texture.width;//textureのwidth格納
        textureHeight = texture.height;//textureのheight格納
        baseWidth = Screen.width;//Screenのwidth格納
        baseHeight = Screen.height;//screenのheight格納
        pieceHeight = GameController.GetComponent<ImageSlicer>().pieceHeight;//pieceのheightをImageSlicerから取得
        pieceWidth = GameController.GetComponent<ImageSlicer>().pieceWidth;//pieceのwidthをImageSlicerから取得
        CameraResize();//カメラのSize変更呼び出し
    }


    /// <summary>
    /// 理想としてはTextureのSizeとPieceのSizeから操作領域サイズを計算して描画サイズを変更したかったが
    /// GameViewの縦横の倍率=windowSizeを直接変更できない(できるのかもしれないがやり方がわからない)
    /// ため妥協としてCameraのOrthoorthographicSizeを変更
    /// ただし、これも正常に動作せず微妙にpieceが画面からはみ出してしまう
    /// </summary>
    void CameraResize()//カメラのSize変更呼び出し
    {
        float heightRatio = (textureHeight+pieceHeight) / baseHeight;//縦比計算
        float widthRatio = (textureWidth+ pieceWidth) / baseWidth;//横比計算
        mainCamera.orthographicSize *= Mathf.Max(widthRatio, heightRatio);//比率修正
    }

}
