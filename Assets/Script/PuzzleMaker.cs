﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// パズル生成クラス
/// </summary>

public class PuzzleMaker : MonoBehaviour
{
    public List<Sprite> piecesSprites = new List<Sprite>();//ピースのSpriteリスト
    public GameObject pieceParent;//ピースの親オブジェクト
    ImageSlicer slicer;//スライサーインスタンス
    public GameObject puzzleArea;//パズル領域オブジェクト
    public Vector2 puzzleSize;//パズルの大きさ
    Texture2D texture;//texture
    int raw;//横分割数
    int column;//縦分割数
    public Texture2D resizedtexture;//サイズ修正後Texture
    GamePlayController gameController;//GamePlayController

    void Start()
    {
        gameController = gameObject.GetComponent<GamePlayController>();//GamePlayController取得
        slicer = gameObject.GetComponent<ImageSlicer>();//ImageSlicerクラス取得
        texture = gameController.texture;//GameControllerからTexture取得
        puzzleSize = new Vector2(texture.width, texture.height);//パズルサイズをTextureの大きさから生成

        column = gameController.column;//縦分割数をGameControllerから取得
        raw = gameController.raw;//横分割数GameControllerから取得
        piecesSprites = slicer.Slicer(texture,raw,column);//Slicerを呼び出してreturnされたものをSpriteリストに格納
        CreatePieces();//ピース生成メソッド呼び出し
    }

    void CreatePieces()//ピース生成メソッド
    {
        foreach (Sprite sp in piecesSprites)//スプライトリストループ
        {
            GameObject pieceParentObject = (GameObject)Instantiate(Resources.Load("Prefabs/PieceParent"));//プレハブ生成
            GameObject piece = pieceParentObject.transform.Find("Piece").gameObject;//子のPiece取得
            GameObject trigger = pieceParentObject.transform.Find("Trigger").gameObject;//子のトリガー取得
            GameObject pieceBace = trigger.transform.Find("PieceBase").gameObject;//子のトリガー背景取得
            piece.name = pieceParent.name;//名前変更
            piece.GetComponent<SpriteRenderer>().sprite = sp;//ピースのスプライトを設定
            Vector2 SpritePicelPerFixedSize = piece.GetComponent<SpriteRenderer>().sprite.rect.size / 100;//サイズ定義
            piece.GetComponent<BoxCollider2D>().size = SpritePicelPerFixedSize;//ピースのコライダーの大きさをSpriteに合わせる
            piece.GetComponent<RectTransform>().sizeDelta = SpritePicelPerFixedSize;//ピースのRectTranceformの大きさをSpriteに合わせる
            pieceParentObject.transform.SetParent(pieceParent.transform,false);//生成したプレハブの親変更
            pieceBace.GetComponent<SpriteRenderer>().size = SpritePicelPerFixedSize;//トリガー背景の大きさ変更
            pieceBace.GetComponent<RectTransform>().sizeDelta = SpritePicelPerFixedSize;//トリガー背景のRectTranceformの大きさ変更
            gameController.pieceList.Add(piece);//ピースリストにピースを追加
            gameController.triggerList.Add(trigger);//トリガーリストにピースを追加
        }
        pieceParent.GetComponent<PieceAlignment>().Alignment();//ピース整列呼び出し
    }

}
