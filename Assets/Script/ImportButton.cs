﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Runtime.Serialization.Formatters.Binary;


/// <summary>
/// 画像読み込みボタン用クラス
/// (書いてからコメントアウト記述までに時間が空きすぎたせいかコメントが不確か
/// 実動作に問題はなし)
/// </summary>

public class ImportButton : MonoBehaviour
{
    string savePath;//セーブデータのパス
    string platformPath;//プラットフォーム用のパス
    string fileName = "";//ファイル名用変数

    //環境による保存Path設定
    private void Awake()
    {

        if (UnityEngine.Application.platform == RuntimePlatform.WindowsEditor)//winEditter時パス
        {
            platformPath = "Assets/Resources/SaveData/";
        }
        if (UnityEngine.Application.platform == RuntimePlatform.WindowsPlayer)//winApplication時パス
        {
            platformPath = UnityEngine.Application.dataPath + "/Data/";
        }

        if (!Directory.Exists(platformPath))//パスのディレクトリがなければ
        {
            Directory.CreateDirectory(platformPath);//フォルダ生成
        }
    }

    //エクスプローラーを開いてファイルを読み込む
    public void ReadFile()
    {

        OpenFileName openFileName = new OpenFileName();//OpenFileNameインスタンス

        openFileName.filter = "PNGファイル|\0*.png\0";//ファイル形式フィルタ定義

        ReadImgFIle(openFileName.ShowDialog());//showDialog呼び出して読み込んだ画像のパスをReatImgFileに渡して実行
        fileName = "";//ファイル名リセット

    }

    //バイナリで読み込んだファイルをTexture2Dに変換
    public void ReadImgFIle(string path)
    {
        FileStream filestream = new FileStream(path, FileMode.Open, FileAccess.Read);//fileStream定義
        fileName = Path.GetFileNameWithoutExtension(path);//パスからファイル名取得
        savePath = platformPath + fileName;//保存先パスをplatform+ファイル名に上書き
        BinaryReader bin = new BinaryReader(filestream);//BinaryReaderインスタンス
        byte[] readBinary = bin.ReadBytes((int)bin.BaseStream.Length);//バイナリ配列をBinaryReaderのBaseStreamから取得
        bin.Close();//BinaryReaderClose
        filestream.Dispose();//filestream修了破棄

        Texture2D texture = CleateTextureFromBinary(readBinary);//Binaryから生成したTextureを格納

        SaveNewPieceBinary(texture);//セーブメソッド呼び出し

    }

    public Texture2D CleateTextureFromBinary(byte[] readBinary)//バイナリデータからTexture生成
    {
        Texture2D texture = new Texture2D(1, 1);//新Texture定義
        texture.LoadImage(readBinary);//loadimage

        return texture;//return
    }


    //作成したバイナリセーブデータの保存
    public void SaveNewPieceBinary(Texture2D texture)
    {
        SavePieceBinary binary = CreateSavePieceBinary(texture);//Textureからセーブデータ型生成

        BinaryFormatter bf = new BinaryFormatter();//バイナリフォーマットインスタンス

        FileStream file = File.Create(savePath);//保存先パスにファイル生成

        try
        {
            bf.Serialize(file, binary);//シリアライズ
        }
        finally
        {
            if (file != null)
            {
                file.Close();//fileStreamをClose
            }
        }

        gameObject.GetComponent<ReadTextureFromBinary>().LoadBinaryForTexture();//画像保存後再読み込み呼び出し

    }

    //読み込んだTexture2DからPNGバイナリのセーブデータを作成
    public SavePieceBinary CreateSavePieceBinary(Texture2D texture)
    {
        SavePieceBinary pieceBinary = new SavePieceBinary();//セーブデータ型インスタンス
        pieceBinary.binaryData = texture.EncodeToPNG();//textureをエンコードして保存
        return pieceBinary;//セーブデータ型return
    }
}