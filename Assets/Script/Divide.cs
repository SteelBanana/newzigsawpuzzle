﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Divide : MonoBehaviour
{
    List<GameObject> pieces;//pieceのGameObjectリスト

    private void Start()
    {
        pieces = gameObject.GetComponent<GamePlayController>().pieceList;//GamePlayControllerからpieceList取得
    }

     public void doDivide()
    {

        foreach (GameObject piece in pieces)//ピースリストループ
        {
            var rondomVector = new Vector2(Random.Range(1.0f, -1.0f), Random.Range(1.0f, -1.0f));//ランダムな方向生成
            rondomVector.Normalize();//正規化(必要？)

            piece.GetComponent<MoveOut>().StartMoveOutCoroutine(rondomVector);//MoveOutクラスのコルーチン呼び出しメソッドにランダムな方向を渡して実行
            
        
        }
          
        
    }
}
