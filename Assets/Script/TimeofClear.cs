﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// クリアタイムの計測クラス
/// </summary>
public class TimeofClear : MonoBehaviour
{
    public GameObject Panel;//表示用パネル

    public float countup = 0.0f;//時間カウント用の変数

    public Text timeText;    //時間を表示するText型の変数

    public void StartTime()//コルーチン起動メソッド
    {
        StartCoroutine("ClearTime");//コルーチン呼び出し
    }

    IEnumerator ClearTime()//時間計測コルーチン
    {
        while (true)//ループ
        {
            countup += Time.deltaTime;//時間をカウントする
            yield return null;
        }
    }
    
    public void StopTime()//コルーチン停止メソッド
    {
        StopCoroutine("ClearTime");//コルーチン停止

        timeText.text = "クリアタイム:" + countup.ToString("f1") + "秒\n";
        timeText.text += "(画面クリックで画像選択に戻る)";


        Panel.SetActive(true);//パネル表示
    }

}
