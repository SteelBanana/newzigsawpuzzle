﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;


/// <summary>
/// ピースをバラバラに移動させるクラス
/// </summary>

public class MoveOut : MonoBehaviour
{
    Vector2 MoveOutVector;//移動方向

    public void StartMoveOutCoroutine(Vector2 vec)//コルーチン開始メソッド
    {
        MoveOutVector = vec;//field変数に引数格納
        StartCoroutine("MoveOutCoroutine");//コルーチン呼び出し
    }

    public void StopMoveOutCoroutine()//コルーチン停止呼び出しメソッド
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;//加速度0に
        StopCoroutine("MoveOutCoroutine");//コルーチン停止
        GameObject parent = gameObject.transform.parent.gameObject;//pieceの親取得
        GameObject trigger = parent.transform.Find("Trigger").gameObject;//親の子のtrigger取得
        trigger.GetComponent<BoxCollider2D>().enabled = true;//triggerのenableをtrueに

    }

    IEnumerator MoveOutCoroutine()//移動コルーチン
    {
        while (true)//ループ
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(MoveOutVector* gameObject.GetComponent<Rigidbody2D>().mass);//移動
            yield return null;
        }

    }
}
