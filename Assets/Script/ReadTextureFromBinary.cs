﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// セーブデータとして保存されているバイナリデータ全てを
/// 読み込んでTexture2Dに変換するクラス
/// </summary>
public class ReadTextureFromBinary : MonoBehaviour
{
    string platformPath;//読み込み先のパス
    List<SavePieceBinary> allSavePieceBinary = new List<SavePieceBinary>();//SaveDataリスト

    // Start is called before the first frame update
    void Awake()
    {
        //パス設定
        if (UnityEngine.Application.platform == RuntimePlatform.WindowsEditor)
        {
            platformPath = "Assets/Resources/SaveData/";
        }
        if (UnityEngine.Application.platform == RuntimePlatform.WindowsPlayer)
        {
            platformPath = UnityEngine.Application.dataPath + "/Data/";
        }

        if (!Directory.Exists(platformPath))//パスのフォルダがなければ
        {
            Directory.CreateDirectory(platformPath);//生成
        }
    }


    //フォルダからすべてのバイナリファイルを読み込みデシリアライズしてSavePieceBinaryリストに格納
    public void LoadBinaryForTexture()
    {

        allSavePieceBinary.Clear();//リストの初期化
        BinaryFormatter bf = new BinaryFormatter();//バイナリフォーマッターのインスタンス

        FileStream file;//ファイルストリーム

        //フォルダ内の全ファイルの内metaDataを除外してリストに格納
        List<string> allBinary = Directory.GetFiles(platformPath, "*", SearchOption.AllDirectories)
                                    .Where(path => !path.EndsWith(".meta"))
                                    .ToList();

        foreach (string path in allBinary)//フォルダ内の非metaDataリストループ
        {
            file = File.Open(path, FileMode.Open);//パスのファイルを開く

            SavePieceBinary pieceBinary = (SavePieceBinary)bf.Deserialize(file);//デシリアライズ

            allSavePieceBinary.Add(pieceBinary);//セーブデータ型配列に格納
            file.Close();//ファイルクローズ
        }

        gameObject.GetComponent<CreateThumbnails>().AddThumbnails(allSavePieceBinary);//サムネイル追加メソッド呼び出し

    }

    
}
