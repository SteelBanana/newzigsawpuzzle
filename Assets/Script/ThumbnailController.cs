﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityScript.Steps;


/// <summary>
/// サムネイル画像をコントロールするクラス
/// </summary>
public class ThumbnailController : MonoBehaviour
{
    Toggle toggle;//選択中用トグル
    private Color selected;
    Color defaultColor;//デフォルトから―



    void Start()
    {
        toggle = gameObject.GetComponent<Toggle>();//トグル定義
        toggle.onValueChanged.AddListener(OnToggleValueChanged);//トグルにonValueChangeにonValueChangeメソッドを追加
        defaultColor= gameObject.GetComponent<RawImage>().color;//デフォルトカラーをRawImageから取得
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(150, 150);//RectTranceform定義
        resizeAspect();//画像のリサイズ
    }

    private void resizeAspect()//画像をリサイズするクラス
    {
        int width = gameObject.GetComponent<RawImage>().texture.width;//画像の元の大きさ(横)取得
        int height = gameObject.GetComponent<RawImage>().texture.height;//画像の元の大きさ(縦)取得

        Vector2 resize = CalcFix(width, height);//リサイズ計算呼び出し

        gameObject.GetComponent<RectTransform>().sizeDelta = resize;//サムネイルのRectTranceformを計算したサイズに変更
    }

    public Vector2 CalcFix(int w, int h)//リサイズ計算
    {
        var baseSize = gameObject.GetComponent<RectTransform>().rect.size;//RectTranceformの大きさを取得
        var widthRatio = baseSize.x / w;//縦比
        var heightRatio = baseSize.y / h;//横比
        var fixedSize = new Vector2(w, h) * Mathf.Min(widthRatio, heightRatio);//比の小さいほうに合わせて大きさを計算

        return fixedSize;//return

    }

    void OnToggleValueChanged(bool isOn)//トグルの状態変更時に呼び出されるクラス
    {
        GameObject GameController = GameObject.FindGameObjectWithTag("GameController");//GameControllerをTagでfindする

        if (isOn)//トグルがOnならば
        {
            gameObject.GetComponent<RawImage>().color = Color.red;//画像の色を赤にする(視覚化)
            GameController.GetComponent<SelectSceneController>().ChooseImage();//画像選択メソッド呼び出し
        }

        else
        {
            gameObject.GetComponent<RawImage>().color = defaultColor;//画像の色をデフォルトに戻す
            GameController.GetComponent<SelectSceneController>().ResetImage();//画像リセットメソッド呼び出し
        }
    }

}
