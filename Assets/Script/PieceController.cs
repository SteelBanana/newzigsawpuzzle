﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ピースの状態を管理するクラス
/// </summary>


public class PieceController : MonoBehaviour
{
    public bool cleared;//クリアフラグ

    public void ColliderOff()//コライダーオフメソッド
    {
        gameObject.GetComponent<BoxCollider2D>().enabled = false;//コライダーoff
        cleared = true;//クリアフラグon
    }

  
}
