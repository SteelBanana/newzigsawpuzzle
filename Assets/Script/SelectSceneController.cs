﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


/// <summary>
/// 画像選択シーンを全体的に
/// コントロールするクラス
/// </summary>

public class SelectSceneController : MonoBehaviour
{

    public List<GameObject> thumbnailImages = new List<GameObject>();//サムネイルリスト
    public static Texture2D texture;//Texture2D(シーンまたぎ用Static変数)
    public GameObject thumbnailDisplay;//選択中の画像を大きく表示しているGameObject
    public static int puzzleColumn;//パズルの大きさ(縦)(シーンまたぎ用Static変数)
    public static int puzzleRaw;//パズルの大きさ(横)(シーンまたぎ用Static変数)
    Vector2 ThumbnailDisplayBaseSize;//サムネイルを大きく表示する用の大きさ

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<ReadTextureFromBinary>().LoadBinaryForTexture();//フォルダから画像読み込むクラス
        ThumbnailDisplayBaseSize= thumbnailDisplay.GetComponent<RectTransform>().rect.size;//画像拡大表示の大きさを取得
    }

    public void SetDifficulty(int difficulty)//難易度設定
    {
        puzzleRaw = difficulty;//引数を分割数(横)に格納
        puzzleColumn = difficulty;//引数を分割数(縦)に格納
    }

    public void ChooseImage()//画像選択メソッド
    {
       foreach(GameObject thumbnail in thumbnailImages)//サムネイルループ
        {
            if (thumbnail.GetComponentInChildren<Toggle>().isOn)//選択中の場合
            {
                if (!thumbnailDisplay.GetComponent<RawImage>().enabled)//拡大表示中でない場合
                {
                    thumbnailDisplay.GetComponent<RawImage>().enabled = true;//拡大表示用オブジェクトのenableをOnに
                }

                texture = (Texture2D)thumbnail.GetComponentInChildren<RawImage>().texture;//選択中のサムネイルのtextureをtexture変数に格納
                ThumbnailDisplay();//拡大表示メソッド実行
            }
        }
    }

    public void ResetImage()//画像リセット
    {
        texture = null;//texture変数を空に
        thumbnailDisplay.GetComponent<RawImage>().enabled = false;//拡大表示オブジェクトのenableをoffに
    }

    public void ThumbnailDisplay()//拡大表示メソッド
    {
        Vector2 resizeForDisplayRatio = CalcFix(texture.width, texture.height);//画像サイズ計算

        thumbnailDisplay.GetComponent<RawImage>().texture = texture;//拡大表示用Objectのtextureの更新
        thumbnailDisplay.GetComponent<RectTransform>().sizeDelta = resizeForDisplayRatio;//拡大表示用Objectのサイズを計算したサイズに
    }

    public Vector2 CalcFix(int w, int h)//リサイズ
    {
        //元のサイズを取得
        var baseSize = ThumbnailDisplayBaseSize;
        var widthRatio = baseSize.x / w;//横比計算
        var heightRatio = baseSize.y / h;//縦比計算
        var fixedSize = new Vector2(w, h) * Mathf.Min(widthRatio, heightRatio);//縦横比の小さい方に合わせる

        return fixedSize;//return

    }

    public void GameStart()//ゲーム開始メソッド
    {
        if (texture!=null && puzzleRaw!=0)//textureとパズルサイズ(難易度)が両方とも空でない場合
        {
            SceneManager.LoadScene("GamePlayScene");//シーン遷移
        }

    }

}
