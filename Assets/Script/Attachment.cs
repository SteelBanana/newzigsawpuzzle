﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// TriggetにPieceを吸着させるクラス
/// </summary>


public class Attachment : MonoBehaviour
{
    //ピースを型にはめる処理
    GameObject piece;//パズルのピースObject

    public GameObject GameController;//GameController

    public bool isPiece = false;//ピースの有無判別フラグ

    private void Awake()//Trigger生成時処理
    {
        GameController = GameObject.FindGameObjectWithTag("GameController");//Find
    }

    private void OnTriggerEnter2D(Collider2D collision)//Triggerにpieceが入ったら
    {
        collision.GetComponent<DragAndDrop>().colliders.Add(gameObject.GetComponent<BoxCollider2D>());//移動クラスのコライダーリストに格納
    }

    private void OnTriggerExit2D(Collider2D collision)//pieceがTriggeから出たら
    {
        collision.GetComponent<DragAndDrop>().colliders.Remove(gameObject.GetComponent<Collider2D>());//移動クラスのコライダーリストから消去
        isPiece = false;//格納フラグfalse
    }

    public void PieceAttachMent(Collider2D collision)//吸着メソッド
    {
        piece = collision.gameObject;//pieceにcollisionのgameObjectを格納

        StartCoroutine("pieceMove");//コルーチン呼び出し
    }

    //ピース移動コルーチン
    IEnumerator pieceMove()
    {
        //ピースとトリガーの位置が違う場合ループ
        while (piece.transform.position != gameObject.transform.position)
        {
            //ピースの位置をトリガーの位置にVector3.MoveTowarsで移動
            piece.transform.position = Vector3.MoveTowards(piece.transform.position, gameObject.transform.position,1.0f);
            yield return null;
        }
        PieceCollectJudge();//ピースの正誤判定
        isPiece = true;//ピース格納フラグtrue
    }

    void PieceCollectJudge()//ピースの正誤判定メソッド
    {
        if (gameObject.transform.parent == piece.transform.parent)//ピースの親とトリガーの親が一緒なら
        {
            gameObject.GetComponent<BoxCollider2D>().enabled = false;//Triggerのコライダー切る
            piece.GetComponent<PieceController>().ColliderOff();//Pieceのコライダー切るメソッド呼び出し
            GameController.GetComponent<ClearCheck>().ClearChecker();//クリアチェック呼び出し
        }
    }


}
