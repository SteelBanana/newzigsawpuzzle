﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// ピースをマウスのドラッグ&ドロップで移動させるクラス
/// </summary>
public class DragAndDrop : MonoBehaviour
{
   
    public Vector3 screenPoint;//スクリーン座標
    public Vector3 offset;//ズレ
    public List<Collider2D> colliders;//コライダーリスト
    public GameObject target;//移動ターゲットオブジェクト


    void OnMouseDown()//クリックしたとき
    {
        //ピースの中心とクリックした座標のズレを計算して格納
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,0));
    }

    private void OnMouseDrag()//マウスドラッグ中
    {
        target = null;//ターゲットnullリセット
        Vector3 currentScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);//マウスのスクリーン座標
        Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenPoint) + this.offset;//+クリック時のズレ補正座標
        gameObject.transform.position = new Vector3(currentPosition.x, currentPosition.y, 0);//pieceの座標を補正済み座標に代入
    }

    private void OnMouseUp()//クリックを離したとき
    {
        if (colliders.Count!=0)//コライダーリストが空でなければ
        {
            TargetJudge();//ターゲット判別メソッド呼び出し
        }
    }

    public void TargetJudge()//ターゲット判別メソッド
    {

        float distance=0;//距離
        foreach (Collider2D col in colliders)//コライダーリストループ
        {
            //トリガーとピースの距離を取得(計算)
            float distance2 = Vector3.Distance(gameObject.transform.position, col.gameObject.transform.position);

            if (target == null)//ループ1回目処理
            {
                target = col.gameObject;//ターゲットにコライダー格納
                distance= Vector3.Distance(gameObject.transform.position, col.gameObject.transform.position);//距離格納
            }
            if (distance2 < distance)//距離が近かったら
            {
                target = col.gameObject;//ターゲット上書き
                distance = distance2;//距離上書き
            }
        }

        if (target.GetComponent<Attachment>().isPiece == false)//targetにピースが入っていなければ
        {
            //ターゲットの吸着メソッド呼び出し(pieceのColliderを渡す)
            target.GetComponent<Attachment>().PieceAttachMent(gameObject.GetComponent<BoxCollider2D>());
        }
    }
}
